FROM python:3.10.9

WORKDIR /code

RUN pip install --upgrade pip
RUN pip install poetry

COPY ./pyproject.toml /code/
COPY ./poetry.lock /code/

ENV PYTHONPATH="/code/app/"

RUN poetry config virtualenvs.create false \
    && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

COPY ./inference.py /code/app/inference.py
COPY ./src /code/app/src
COPY ./__init__.py /code/app/
COPY ./.env /code/app/.env

CMD ["uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]