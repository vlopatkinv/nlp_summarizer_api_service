import os
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import JSONResponse
from dotenv import load_dotenv
import mlflow
import pandas as pd
import requests

import nltk
nltk.download('stopwords')
nltk.download('punkt')

load_dotenv()

mlflow_tracking_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(mlflow_tracking_uri)

# tgbot credentials
tg_api_token = os.getenv("TGBOT_API_KEY")
tg_chat_id = os.getenv("TG_CHAT_ID")

def send_to_telegram(message):
    apiURL = f'https://api.telegram.org/bot{tg_api_token}/sendMessage'

    try:
        response = requests.post(apiURL, json={'chat_id': tg_chat_id, 'text': message})
        print(response.text)
    except Exception as e:
        print(e)

# init fastapi
app = FastAPI()

class Model:
    def __init__(self, model_name, model_stage):
        """
        To init model
        model_name: name of model in registry
        model_stage: Stage of the model
        """
        # Load the model from Registry
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")
        import inspect
        # print('\nCLASS:', self.model.__class__)
        # print('\nTYPE:', type(self.model))
        # print('\nDIR:', dir(self.model))
        
    def predict(self, data):
        """
        Use the loaded model to make predictions
        data: dataset to predict on
        """
        # Make prediction on provided data
        print('\nSIG:', inspect.signature(self.model.predict))
        prediction = self.model.predict(data)
        return prediction
    
# Create model
model_regressor = Model("test_diabetes_RFregressor", "Production")
model_TextRank = Model("TextRank", "Production")

import inspect
# print('\nCLASS:', model_TextRank.__class__)
# print('\nTYPE:', type(model_TextRank))
# print('\nDIR:', dir(model_TextRank))
# print('\nSIG:', inspect.signature(model_TextRank.predict))

# Create POST endpoint with path '/invocations'
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith('.ogg'):
        return f"Voice file received. Filename {file.filename}"
    
    if file.filename.endswith('.txt'):
        # Create temp file to containt input
        with open(file.filename, 'wb') as f:
            f.write(file.file.read())  
            
        with open(file.filename, 'r') as f:
            input_text = f.readlines()

        data = " ".join(input_text)

        os.remove(file.filename)
        # print('\nDATA:', data)
        # print('\nTYPE', type(data))
        prediction = model_TextRank.predict(pd.DataFrame(data=dict(raw_text=[data])))
        # print('\nRESULT_TYPE', type(prediction), '\nRESULT', prediction)
        # return list(prediction)

        # todo: remove
        # send the result back to telegram chat
        # send_to_telegram(f'debug response: {prediction}')

        response = {
        "prediction": prediction
        }

        # Return the response as JSON
        return JSONResponse(content=response)
        # return list(prediction)
    
    if file.filename.endswith('.csv'):
        # Create temp file to containt input
        with open(file.filename, 'wb') as f:
            f.write(file.file.read())     
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        prediction = model_regressor.predict(data)

        response = {"prediction":prediction}
        return list(prediction)
    
    else:
        raise HTTPException(status_code=400, detail='Invalid format, should be .ogg file')